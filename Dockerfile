# Use the node 8 base image alpine distribution
FROM node:8-alpine

LABEL maintainer "Sachin Goel <sachin.goel@indiqus.com>"

# Mark workdir as /app
WORKDIR /app

# Copy package.json and packago-lock.json in advance to maintain the Docker cache in case of code change
COPY package.json /app

# Install all the dependencies
RUN npm install

# Copy the app content
COPY . .

# Expose port 1337 for the application to listen to client requests
EXPOSE 1337

CMD ["node", "index.js"]